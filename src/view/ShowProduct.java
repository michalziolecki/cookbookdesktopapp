package view;

import java.util.List;

import application.DataBase;
import application.Product;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.css.converter.EnumConverter;
import javafx.scene.Node;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.util.Callback;

public class ShowProduct {

	private TableView<Product> tableWithProducts;
	private ObservableList<Product> listOfProduct;
	private List<Product> temporaryList;
	DataBase dataBase = new DataBase();
	
	public ShowProduct()
	{
		showProductInMainWindow();
	}
	
	public void showProductInMainWindow()
	{
		//tworzenie tabeli i listy
		tableWithProducts = new TableView<>();
		tableWithProducts.setLayoutX(350);
		tableWithProducts.setLayoutY(70);
		temporaryList = dataBase.loadProducts();
		listOfProduct = FXCollections.observableArrayList(temporaryList);
		tableWithProducts.setItems(listOfProduct);
		tableWithProducts.itemsProperty();
		
		//tworzenie kolumn
		tableWithProducts.setEditable(true);
		TableColumn<Product, String> nameCol = new TableColumn<Product,String>("Nazwa produktu");
		nameCol.setMinWidth(150);
		nameCol.setCellValueFactory(new PropertyValueFactory<Product,String>("name"));
		tableWithProducts.getColumns().add(nameCol);
		
		TableColumn<Product,Integer> kcalCol = new TableColumn<Product,Integer>("Kalorie");
		kcalCol.setMinWidth(150);
		kcalCol.setCellValueFactory(new PropertyValueFactory<Product,Integer>("kcal"));
		
		kcalCol.setCellFactory(column -> {
		    return new TableCell<Product, Integer>() {
		        @Override
		        public void updateItem(Integer item, boolean empty) {
		        super.updateItem(item, empty);
		        //this.setItem(item);
		        String itemS = "" + item;
		        this.setText(itemS);
	
		        	//setItem(item);
		        	if(item != null )
		        	{
	                    setStyle("-fx-background-color: red");
	                    
	                    setTextFill(Color.BLACK);
	                    
		        	}
		        	else {
		        		//setTextFill(Color.BLACK); 
	                    setStyle("-fx-background-color: white");
		        	}
		        	
		        	
		        }
		    };
		});
		tableWithProducts.getColumns().add(kcalCol);
		
		ObservableList<Node> children = ViewManager.mainPane.getChildren();
		if(children.size() > 5) {
		children.remove(children.size()-1);
		}
		ViewManager.mainPane.getChildren().add(tableWithProducts);
		//sprawdzanie zwalniania pamieci czy dziala
		//System.out.println(ViewManager.mainPane.getChildren().size());
	}
	
}
