package view;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;


import application.DataBase;
import javafx.animation.AnimationTimer;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class AddProduct {
	
	private final static String FONT = "Arial";
	private final static String ADD_PRODUCT_NAME = "Dodaj nowy produkt";
	String temporaryNameOfProduct = null;
	String temporaryKcalOfProduct = null;
	DataBase dataBase = new DataBase();
	//AnimationTimer menuTimer;
	
	
	public AddProduct ()
	{
		createAndShowAddProductStage();
	}

	//------GUI okna dodawania produktow
	private void createAndShowAddProductStage()
	{
		// Tworzenie okna dodawania produktow
		AnchorPane pane = new AnchorPane();
		Scene scene =  new Scene(pane, 400, 200);
		Stage stage = new Stage();
		stage.setScene(scene);
		stage.setTitle(ADD_PRODUCT_NAME);
		stage.setResizable(false); // nie mozna zmieniac wielkosci
		//przypisuje wlasciciela (matke) okna - jest zawsze na wierzchu
		stage.initOwner(ViewManager.mainStage); 
		//Uniemozliwia otwarcie innych okien apki
		stage.initModality(Modality.WINDOW_MODAL);
		
		
		// Tworzenie elementow sceny
		TextField nameTextField = new TextField();
		nameTextField.setLayoutX(200);
		nameTextField.setLayoutY(40);
		pane.getChildren().add(nameTextField);
		
		TextField kcalTextField = new TextField();
		kcalTextField.setLayoutX(200);
		kcalTextField.setLayoutY(90);
		pane.getChildren().add(kcalTextField);
		
		Label nameLabel = new Label("Podaj produkt: ");
		nameLabel.setFont(new Font(FONT,12));
		nameLabel.setLayoutX(60);
		nameLabel.setLayoutY(45);
		pane.getChildren().add(nameLabel);
		
		Label kcalLabel = new Label("Podaj kaloryke (100g): ");
		kcalLabel.setFont(new Font(FONT,12));
		kcalLabel.setLayoutX(60);
		kcalLabel.setLayoutY(95);
		pane.getChildren().add(kcalLabel);
		
		Label warningLabel = new Label();
		warningLabel.setFont(new Font(FONT,12));
		warningLabel.setLayoutX(40);
		warningLabel.setLayoutY(10);
		
		Label warningLabel2 = new Label();
		warningLabel2.setFont(new Font(FONT,12));
		warningLabel2.setLayoutX(200);
		warningLabel2.setLayoutY(70);
		
		
		Button saveButton = new Button("Zapisz");
		saveButton.setLayoutX(70);
		saveButton.setLayoutY(150);
		saveButton.setPrefWidth(100);
		pane.getChildren().add(saveButton);
		
		Button cancelButton = new Button("Anuluj");
		cancelButton.setLayoutX(220);
		cancelButton.setLayoutY(150);
		cancelButton.setPrefWidth(100);
		pane.getChildren().add(cancelButton);
		
		// czyszczenie wariningLabel da nazwy
		nameTextField.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				
				if(event.getClickCount() == 1 && pane.getChildren().size() >=7)
				{
					
					pane.getChildren().remove(pane.getChildren().indexOf(warningLabel));
				}
			}
		} 
		);
		// czyszczenie wariningLabeldla calorii
		kcalTextField.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
			
			@Override
			public void handle(MouseEvent event) {
				
				if(event.getClickCount() == 1 && pane.getChildren().size() >=7)
				{
					pane.getChildren().remove(pane.getChildren().indexOf(warningLabel2));
				}
			}
		} 
		);
		
		saveButton.setOnAction(new EventHandler<ActionEvent>(){

			@Override
			public void handle(ActionEvent arg0) {
				boolean checkStatus = true;
				temporaryNameOfProduct = nameTextField.getText();
				
				//zabezpieczenie przed pustym tekstem
				if(temporaryNameOfProduct.length() >=1)checkStatus = dataBase.checkStatusForNameOfProduct(nameTextField.getText());
				
				if(checkStatus == false)
				{
					// zabezpieczenie przed podaniem pustego stringa w ilosci kalorii
					temporaryKcalOfProduct = kcalTextField.getText();  
					if( temporaryKcalOfProduct.length() >= 1 )
					{
						dataBase.saveProductInDataBase(nameTextField.getText(), Integer.parseInt(kcalTextField.getText()));
						stage.close();
					}
					else { 
						warningLabel2.setText("Uzupe�nij pole: ");
						pane.getChildren().add(warningLabel2);
					}
				}
				else
				{	
					//tu wchodze gdy jest produkt w bazie lub string jest pusty 
					warningLabel.setText("Pusty produkt lub istnieje w bazie, wpisz inna nazwe/produkt:");
					pane.getChildren().add(warningLabel);
					
					/*final Task task = new Task() {
						@Override
						protected Object call() throws Exception {
							System.out.println("before sleep");
							Thread.sleep(2000);
							System.out.println("after sleep");
							pane.getChildren().remove(pane.getChildren().indexOf(warningLabel));
							System.out.println("remove pane");
							stage.show();
							return null;
						}
					};
					Thread thread = new Thread(task);
					thread.start();*/
				}
			}
		});
		
		cancelButton.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {			
				stage.close();
			}
		});
		
		stage.show(); // wyswietlenie okna		
	}
	
	
	
}
