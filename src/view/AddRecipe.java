package view;

import java.util.ArrayList;
import java.util.List;

import javax.management.modelmbean.ModelMBeanInfoSupport;

import application.DataBase;
import application.Product;
import application.Recipe;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCharacterCombination;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class AddRecipe {

	//stale 
	private final static String FONT = "Arial";
	private final static String ADDRECIPE_NAME = "Dodaj przepis";
	private final static String NAMELABEL = "Podaj nazwe:";
	
	//pola i przyciski
	TextField nameTextField = new TextField();
	TextField gramTextField;
	Label nameLabel = new Label(NAMELABEL);
	
	Button saveButton = new Button("Zapisz");
	Button cancelButton = new Button("Anuluj");
	//Button addGramsButton;
	
	//bazy danych i reprezentacje tabelaryczne
	DataBase dataBase = new DataBase();
	
	private TableView<Product> productsTableToAddRecipe;
	ObservableList<Product> productsListToAddRecipe;
	AnchorPane pane = new AnchorPane();
	String temporaryNameOfRecipe;
	
	
	
	public AddRecipe()
	{
		createAndShowAddRecipeStage();	
	}
	
	private void createAndShowAddRecipeStage()
	{
		//Tworzenie okna
		
		Scene scene =  new Scene(pane, 600, 600);
		Stage stage = new Stage();
		stage.setScene(scene);
		stage.setResizable(false);
		stage.setTitle(ADDRECIPE_NAME);
		stage.initOwner(ViewManager.mainStage);
		stage.initModality(Modality.WINDOW_MODAL);
		
		//elementy okna
		productsListToAddRecipe = FXCollections.observableArrayList();
		productsTableToAddRecipe = new TableView<>();
		productsTableToAddRecipe.setLayoutX(300);
		productsTableToAddRecipe.setLayoutY(100);
		//pane.getChildren().add(productsTableToAddRecipe);
		List<Product> temporaryList = dataBase.loadProducts();
		productsListToAddRecipe = FXCollections.observableArrayList(temporaryList);
		productsTableToAddRecipe.setItems(productsListToAddRecipe);
		loadColumnsForProducts();
		
		
		nameTextField.setLayoutX(250);
		nameTextField.setLayoutY(50);
		pane.getChildren().add(nameTextField);	
		
		nameLabel.setLayoutX(150);
		nameLabel.setLayoutY(50);
		nameLabel.setFont(new Font (FONT, 15));
		pane.getChildren().add(nameLabel);
		
		saveButton.setLayoutX(200);
		saveButton.setLayoutY(550);
		saveButton.setPrefWidth(100);
		pane.getChildren().add(saveButton);
		
		cancelButton.setLayoutX(350);
		cancelButton.setLayoutY(550);
		cancelButton.setPrefWidth(100);
		pane.getChildren().add(cancelButton);
		
		Label warningLabel = new Label();
		warningLabel.setFont(new Font(FONT,12));
		warningLabel.setLayoutX(150);
		warningLabel.setLayoutY(20);
		
		Label warningLabel2 = new Label();
		warningLabel2.setFont(new Font(FONT,12));
		warningLabel2.setLayoutX(150);
		warningLabel2.setLayoutY(5);
		
		//czyszczenie label warning po wroceniu do pola
		nameTextField.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				if(event.getClickCount() == 1 && pane.getChildren().size() >=6)
				{
					// zeby nie usuwal gdy go nie ma (warningLabel)
					if(pane.getChildren().indexOf(warningLabel)>=5)
					{
						pane.getChildren().remove(pane.getChildren().indexOf(warningLabel));
					}
				}
			}
		});
		
		//zachowanie okna pod wplywem akcji
		List<Product> listOfProductAddToRecipe = new ArrayList<Product>();
		
		//--zapisanie do bazy - przez dwukrotne klikniecie i dodanie gramatury enterem
		productsTableToAddRecipe.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				if(event.getClickCount() == 2)
				{
					if(pane.getChildren().size() >=6)
					{
						pane.getChildren().remove(pane.getChildren().indexOf(warningLabel2));
					}
					Product product = productsListToAddRecipe.get(productsTableToAddRecipe.getSelectionModel().getSelectedIndex());
					Label addProductLabel = new Label();
					addProductLabel.setLayoutX(30);
					addProductLabel.setLayoutY(180);
					addProductLabel.setFont(new Font(FONT,15));
					addProductLabel.setText("Dodano do przepisu: " + product.getName()+ "\n \n Podaj ilosc gram produktu"
							+ " \n potem wcisnij Enter:");
					pane.getChildren().add(addProductLabel);
					//----------dodawanie gram
					gramTextField =  new TextField();
					gramTextField.setLayoutX(60);
					gramTextField.setLayoutY(270);
					pane.getChildren().add(gramTextField);
					
					gramTextField.setOnKeyPressed(new EventHandler<KeyEvent>() 
					{
						@Override
						public void handle(KeyEvent keyEvent) 
						{
							if(keyEvent.getCode() == KeyCode.ENTER)
							{
								Integer grams;
								grams = Integer.parseInt(gramTextField.getText());
								product.setGram(grams);
								listOfProductAddToRecipe.add(product);
								//System.out.println("w obiekcie product: " + listOfProductAddToRecipe.get(0));
								pane.getChildren().remove(pane.getChildren().indexOf(gramTextField));
								pane.getChildren().remove(pane.getChildren().indexOf(addProductLabel));
							}
						}
					});
					//---------
				}
				
			}
		});
		
		saveButton.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent arg0) {
				// tutaj dodawanie produktow do bazy
				boolean checkStatus = true;
				temporaryNameOfRecipe = nameTextField.getText();
				checkStatus = dataBase.checkStatusForNameOfRecipe(nameTextField.getText());
				if(temporaryNameOfRecipe.length() >=1 && checkStatus == false)
				{
					// zabezpieczenie przed dodaniem przepisu bez produktow
					if(listOfProductAddToRecipe.size() > 0)
					{
						dataBase.saveRecipeInDataBase(nameTextField.getText());
						Recipe loadedrecipe = dataBase.readRecipeFromDataBase(nameTextField.getText());
						
						dataBase.saveProductsForRecipe(loadedrecipe, listOfProductAddToRecipe);
						stage.close();
					}
					else
					{
						warningLabel2.setText("Jak to przepis bez produktow? :-) \n Dodaj je klikaj�c dwukrotnie na produkt");
						pane.getChildren().add(warningLabel2);
					}
						
				}
				else 
				{
					warningLabel.setText("Puste pole przepisu lub istnieje on w bazie, wpisz inna nazwe: ");
					pane.getChildren().add(warningLabel);
					System.out.println("Rozmiar: " + pane.getChildren().size());
				}
			}
		});
		//--zamniecie okna 
		cancelButton.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				stage.close();	
			}
		});
		stage.show();
	
	}
	
	private void loadColumnsForProducts()
	{
		//pane.getChildren().remove(productsTableToAddRecipe);
		productsTableToAddRecipe.setEditable(true);
				
		TableColumn<Product, String> nameCol = new TableColumn<>("Nazwa produktu");
		nameCol.setMinWidth(150);
		nameCol.setCellValueFactory(new PropertyValueFactory<Product,String>("name"));
		productsTableToAddRecipe.getColumns().add(nameCol);	
				
		TableColumn<Product, Integer> kcalCol = new TableColumn<>("kalorie");
		kcalCol.setMinWidth(100);
		kcalCol.setCellValueFactory(new PropertyValueFactory<Product, Integer>("kcal"));
		productsTableToAddRecipe.getColumns().add(kcalCol);
				
		pane.getChildren().add(productsTableToAddRecipe);
	}	
}
